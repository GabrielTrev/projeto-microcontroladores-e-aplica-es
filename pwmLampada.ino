const int pinoSensor = A0;
const int pinoPWM = 3;
int passo = 1; //Recebe -1 para decremento e 1 para incremento
int pwm = 0;
int contadorLeitura = 0;
int contadorLoop = 0;
float temperatura = 0;
long inicio;

void setup()
{
  pinMode(pinoSensor, INPUT);
  pinMode(pinoPWM, OUTPUT);
  analogReference(INTERNAL);
  Serial.begin(9600);
}

void loop()
{
  inicio = millis();
  
  while(millis() - inicio < 10)
  {
    temperatura += analogRead(pinoSensor)/9.31;
    contadorLeitura++;
  }
  
  if (pwm == 255) {passo = -1;}
  else if (pwm == 0) {passo = 1;}
  pwm += passo;
  analogWrite(pinoPWM,pwm);
  
  if (++contadorLoop == 100)
  {
    temperatura /= contadorLeitura;
    if (Serial.available())
    {
      Serial.println(temperatura);
    }
    contadorLeitura = 0;
    contadorLoop = 0;
    temperatura = 0;
  }
}
