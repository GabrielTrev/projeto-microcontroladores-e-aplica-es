# Projeto Lab. Microcontroladores e Aplicações

## Proposta de projeto:
Este projeto consistirá de um controlador PID da temperatura de uma lâmpada incandescente.

A medição da temperatura será feita por um sensor de temperatura (LM-35), cuja saída é linear em relação à temperatura em graus centígrados. Para obter o resultado da medição, a saída desse sensor será conectada à entrada analógica do Arduino, que traduzirá o valor lido na temperatura aferida.

Para o controle da temperatura, o usuário definirá um set point, que será mostrado em um display LCD, juntamente com o valor de temperatura lido.

Com base na diferença entre o set point e a temperatura lida, o controlador irá enviar um sinal PWM para os terminais da lâmpada, acionando-a de forma que sua temperatura de altere da forma adequada.

## Itens utilizados:

* Arduino UNO
* Lâmpada incandescente
* Sensor de temperatura LM-35 ([datasheet](http://pdf1.alldatasheet.com/datasheet-pdf/view/8866/NSC/LM35.html))
* Dois botões (para incremento e decremento da temperatura do set-point)
* Display LCD 16x2
