#include <rgb_lcd.h>
#include <Wire.h>

rgb_lcd lcd;

const int pinoSensor = A0;
int passo = 1; //Recebe -1 para decremento e 1 para incremento
int contador = 0;
float temperatura = 0;
long inicio;

void setup()
{
  pinMode(pinoSensor, INPUT);
  analogReference(INTERNAL);
  lcd.begin(16,2);
  lcd.setRGB(255,0,0);
  Serial.begin(9600);
}

void loop()
{
  inicio = millis();

  while(millis() - inicio <= 1000)
  {
    temperatura += analogRead(pinoSensor)/9.31;
    contador++;
  }
  temperatura /= contador;
  
  //temperatura = analogRead(pinoSensor)/9.31;
  
  lcd.setCursor(0,0);
  lcd.print("Temperatura = ");
  lcd.setCursor(0,1);
  lcd.print(temperatura);
  if(Serial.available())
  {
    Serial.println(temperatura);
  }

   contador = 0;
   temperatura = 0;
}
