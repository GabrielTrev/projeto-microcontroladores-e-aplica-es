// Inclusao das bibliotecas utilizadas
#include <PID_v1.h>
#include <rgb_lcd.h>
#include <Wire.h>
#include <PID_AutoTune_v0.h>

// Declaracao do display LCD RGB utilizado
// Os pino A4 e A5 serao utilizados para a interface com o display
// O pino A4 sera utilizado para envio de dados e o pino A5 para o 
// clock
rgb_lcd lcd;

const int PRIMEIRA_LEITURA = 255;

const int pinoSensor = A0; // Pino do sensor de temperatura
const int pinoPWM = 5; // Pino de saida do PWM para a lampada
const int pinoLedTuning = 4; // Pino para led que indica autotune
const int pinoBotaoUp = 2; // Pino para o botao de incrementar setpoint
const int pinoBotaoDown = 3; // Pino para o botao de decrementar setpoint
const int pinoBotaoTuning = 18; // Pino para configurar o autotune

volatile int flagUp = LOW;
volatile int flagDown = LOW;
volatile int flagTuning = LOW;

// Variaveis de entrada e saida para o PID
double pwm = 0; // Valor do PWM que sera enviado para o transistor
double setpoint = 50; // setpoint para o PID
double temperaturaAcc = 0; // Acumulador para leitura de temperatura
// Ultima temperatura lida
// Na primeira leitura, deve ser desconsiderada para PID
double temperatura = PRIMEIRA_LEITURA;

// Constantes para PID (Kp, Ki e Kd)
//double constKp =1.5, constKi =3, constKd =0.5;
double constKp =3, constKi =5, constKd =2;
// Valores usados como parametros para a rotina de autotune
double aTuneModeRemember = 2, aTuneStep = 50, aTuneNoise = 0.05, aTuneStartValue = 90;
unsigned int aTuneLookBack = 20;
// Variavel binaria, determinando se o tuning deve ocorrer
boolean tuning = false;
byte ATuneReturn = 0;
// Construtores do PID e do autotune do PID
// Entrada: temepratura lida
// Saida: PWM para a lampada
// Setpoint: definido pelo usuario (inicialmente 50)
PID PIDTemp(&temperatura, &pwm, &setpoint, constKp, constKi, constKd, DIRECT);
PID_ATune aTune(&temperatura, &pwm);

long inicio; // Variavel usada para controle do tempo

// Contador usado para acompanhamento do acumulador
int contadorLeitura = 0;
int contadorLoop = 0; // Contador de loops de 10ms

// Prototipos para as funcoes utilizadas
void upSetpoint(void);
void downSetpoint(void);
void setTuning(void);
void updateBacklight(void);

// Funcao de configuracao do arduino e inicializacao do LCD
void setup()
{
  // Inicializacao dos pinos
  pinMode(pinoBotaoUp, INPUT_PULLUP);
  pinMode(pinoBotaoDown, INPUT_PULLUP);
  pinMode(pinoBotaoTuning, INPUT_PULLUP);
  
  pinMode(pinoSensor, INPUT);
  pinMode(pinoPWM, OUTPUT);
  pinMode(pinoLedTuning, OUTPUT);
  
  // Inicializacao do LCD de 16 colunas por 2 linhas
  lcd.begin(16,2);
  // Configuracao da cor inicial do backlight
  lcd.setRGB(0,0,0);
  
  // Configuracao da referencia da leitura analogica para 1,1V
  // Funciona apenas para o Arduino UNO
  analogReference(INTERNAL1V1);
  
  // Vinculacao dos interrupts aos pinos dos botoes
  attachInterrupt(digitalPinToInterrupt(pinoBotaoUp),upSetpoint,RISING);
  attachInterrupt(digitalPinToInterrupt(pinoBotaoDown),downSetpoint,RISING);
  attachInterrupt(digitalPinToInterrupt(pinoBotaoTuning), setTuning, RISING);

  // Inicialização do PID
  PIDTemp.SetMode(AUTOMATIC);

  tuning = false;

  /*// Se autotune estiver configurado para iniciar no começo da execução, 
  // configura os parametros para a rotina de autotune
  if (tuning)
  {
    tuning = false;
    configuraAutoTune();
    tuning = true;
  }*/

  // Inicialização da interface serial
  Serial.begin(9600);
}


void loop()
{
  // Determinacao do tempo de inicio do loop
  inicio = millis();

  // Realiza leituras durante 10ms
  while (millis() - inicio < 10)
  {
    // Leitura do sensor de temperatura e conversao para valor em ºC:
    // Passo do sensor: 10mV/ºC
    // Referencia de leitura analogica: 1.1V
    // Precisao da leitura: 10 bits ->
    // Passo do pino analogico: 1.1/1024 = 0.001742V = 1.742mV
    // Fator de conversao = 10/1.742 = 9.31/ºC ->
    // Temperatura = Leitura / 9.31
    
    // Acumula-se o valor das leituras para determinacao da media
    temperaturaAcc += analogRead(pinoSensor)/9.31;
    
    // Incrementa-se o contador de leituras apos isso
    contadorLeitura++;

    // Incrementa setpoint de 0.5 graus se botao de up foi pressionado
    if (flagUp)
    {
      setpoint += 0.5;
      flagUp = LOW;
    }
  
    // Decrementa setpoint de 0.5 graus se botao de down foi pressionado
    if (flagDown)
    {
      setpoint -= 0.5;
      flagDown = LOW;
    }

    // Escrita do setpoint no LCD
    lcd.setCursor(0,0);
    lcd.print("Setpoint = ");
    lcd.print(setpoint);
    
    // Determinacao e escrita do valor de saida do PWM a partir do PID
    // Desconsidera-se a primeira leitura
    if (temperatura != PRIMEIRA_LEITURA)
    {
      // Verifica se o programa esta configurado para autotune
      if (tuning)
      {
        // Verifica a partir da funcao Runtime se o autotune precisa ser executado
        ATuneReturn = (aTune.Runtime());
        
        // Se o valor de retorno for 1 (autotune completo), modifica tuning para false
        if (ATuneReturn != 0)
        {
          tuning = false;
        }
        
        // Se tuning for false, configura o PID com os parametros kp, ki e kd
        // encontrados pelo autotune
        //Serial.println(tuning);
        if (!tuning)
        {
          constKp = aTune.GetKp();
          constKi = aTune.GetKi();
          constKd = aTune.GetKd();
          PIDTemp.SetTunings(constKp,constKi,constKd);
          AutoTuneHelper(false);
        }
        else {digitalWrite(pinoLedTuning, HIGH);}
      }
      
      // Caso o programa nao esteja confiurado para autotune, computa o PID
      else
      {
        digitalWrite(pinoLedTuning, LOW);
        PIDTemp.Compute();
      }
      // Escrita do PWM para o pino do transistor
      analogWrite(pinoPWM, pwm);

      // Reconfigura o autotune se o botao de autotune foi pressionado
      if (flagTuning)
      {
        configuraAutoTune();
        flagTuning = LOW;
      }

      // Atualizacao do backlight
      updateBacklight();
    }
  }
  
  // A cada 1s, escreve-se a temperatura media medida
  // Tempo de loop = 10ms -> contador = 100
  if (++contadorLoop == 25)
  {
    // Media = Soma / contador
    temperatura = temperaturaAcc/contadorLeitura;
    
    // Escrita da temperatura no LCD
    lcd.setCursor(0,1);
    lcd.print("Leitura = ");
    lcd.print(temperatura);
    
    // Escrita do valor da temperatura na interface serial
    if (Serial.available())
    {
      Serial.print(temperatura);
      Serial.print("\t");
      Serial.println(pwm);
    }
    
    updateBacklight();
    
    // Reinicio dos contadores e da temperatura acumulada
    contadorLeitura = 0;
    contadorLoop = 0;
    temperaturaAcc = 0;
  }
}

// ISR para indicar pressionamento do botao de up
// Aciona flag de up para incrementar setpoint
void upSetpoint(void)
{
  flagUp = HIGH;
}

// ISR para indicar pressionamento do botao de down
// Aciona flag de down para decrementar setpoint
void downSetpoint(void)
{
  flagDown = HIGH;
}

// ISR para indicar pressionamento do botao de autotune
// Liga ou desliga a flag de tuning dependendo de seu estado anterior
void setTuning(void)
{
  flagTuning = HIGH;
}

/* Funcao configuraAutoTune
 * funcao adaptada da funcao changeAutoTune do codigo de exemplo da biblioteca 
 * de autotune do PID
 * Modifica a variavel de tuning e configura os parametros do autotune
 * pwm: variavel de saida do PID
 * tuning: variavel indicadora se o autotune esta sendo utilizado
 * aTuneStartValue: valor inicial da saida
 * aTuneStep: valor da variacao periodica da saida pelo autotune
 * aTuneNoise: valor considerado como ruido para a entrada
 * aTuneLookBack: intervalo de tempo considerado para a determinacao de maximos e 
 * minimos
*/
void configuraAutoTune()
{
 // Se o autotune nao estive iniciado, inicia o autotune com os parametros configurados
 if(!tuning)
  {
    pwm=aTuneStartValue;
    aTune.SetNoiseBand(aTuneNoise);
    aTune.SetOutputStep(aTuneStep);
    aTune.SetLookbackSec((int)aTuneLookBack);
    AutoTuneHelper(true);
    tuning = true;
  }
  // Caso contrario, finaliza o autotune
  else
  {
    aTune.Cancel();
    tuning = false;
    AutoTuneHelper(false);
  }
}

/* Funcao AutoTuneHelper
 * Funcao presente no codigo de exemplo da biblioteca de autotune do PID
 * Le e escreve o modo do PID (automatico ou manual)
 */
void AutoTuneHelper(boolean start)
{
  if(start)
    aTuneModeRemember = PIDTemp.GetMode();
  else
    PIDTemp.SetMode(aTuneModeRemember);
}

// Funcao updateBacklight: atualiza a cor do backlight 
// de acordo com a temperatura. A matiz da cor é variada
// conforme a diferenca entre a temperatura lida e o 
// setpoint
void updateBacklight(void)
{
  int red, green, blue; // Cores do backlight
  int dif = temperatura - setpoint;

  // Cores para temperatura - setpoint < 5
  if (dif < -5)
  {
    red = 0;
    green = 0;
    blue = 255;
  }

  // Cores para -5 <= temperatura - setpoint < 0
  else if (dif >= -5 && dif < 0)
  {
    red = 0;
    green = (dif + 5) * 255/5.0;
    blue = -dif * 255/5.0;
  }

  // Cores para 0 <= temperatura - setpoint < 5
  else if (dif >= 0 && dif < 5)
  {
    red = dif * 255/5.0;
    green = (5 - dif) * 255/5.0;
    blue = 0;
  }

  // Cores para temperatura - setpoint >= 5
  else if (dif >= 5)
  {
    red = 255;
    green = 0;
    blue = 0;
  }

  // Escrita das cores no backlight do LCD
  lcd.setRGB(red,green,blue);
}
